import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import Qt.labs.platform 1.0

import Queye 1.0


ApplicationWindow {
    visible: true
    width: 1000
    height: 480
    title: qsTr("Queye")

    FontLoader {
        id: awesomesolid
        source: "qrc:/qml/fa-solid-900.ttf"
        //        source: "qrc:/qml/fa-regular-400.ttf"
    }

    FaVars {
        id: fa
    }


    header: ToolBar {
        leftPadding: 8

        Flow {
            id: flow
            width: parent.width

            RowLayout {
                id: connectRow
//                anchors.fill: parent


                ToolButton {
//                    contentItem: FaIcon {
//                        text: fa.fa_bolt
//                    }
                    font.family: awesomesolid.name
                    text: fa.fa_bolt

                    visible: !cam.connected
                    onClicked: cam.connect()
                }

                ToolButton {
//                    contentItem: FaIcon {
//                        text: fa.fa_times
//                    }
                    font.family: awesomesolid.name
                    text: fa.fa_times


                    visible: cam.connected
                    onClicked: cam.disconnect()
                }

                Label {
                    text: cam.connected ? cam.model + " - " + cam.sizeX + " x " + cam.sizeY : "No Camera"
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    //                Layout.fillWidth: true
                }

                ToolSeparator {
                    contentItem.visible: connectRow.y == controlRow.y
                }
            }

            RowLayout {
                id: controlRow
                ToolButton {
                    font.family: awesomesolid.name
                    text: fa.fa_camera
                    enabled: cam.connected
                    onClicked: cam.freezeImage()
                }

                ToolButton {
                    font.family: awesomesolid.name
                    text: fa.fa_video
                    enabled: cam.connected && !cam.capturing
                    onClicked: cam.startCapture()
                }

                ToolButton {
                    font.family: awesomesolid.name
                    text: fa.fa_stop
                    enabled: cam.connected && cam.capturing
                    onClicked: cam.endCapture()
                }

                ToolSeparator {
                    contentItem.visible: controlRow.y == saveRow.y
                }
            }

            RowLayout {
                id: saveRow

                ToolButton {
                    font.family: awesomesolid.name
                    text: fa.fa_save
                    //                visible: cam.connected && cam.capturing
                    onClicked: fileDialog.open();
                    enabled: cam.navgs > 0

                }

                ToolSeparator {
                    contentItem.visible: saveRow.y == avgsRow.y
                }
            }


            RowLayout {
                id: avgsRow
                Label {
                    text: "Avgs: " + cam.navgs
                    //                horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    //                Layout.fillWidth: true
                }

                ToolButton {
                    font.family: awesomesolid.name
                    text: fa.fa_undo

                    onClicked: cam.resetAvgs()

                }

                Switch {
                    text: "live"
                    checked: !cam.viewAvgs
                    onToggled: cam.viewAvgs = !checked
                }

                ToolSeparator {
                    contentItem.visible: settingsRow.y == avgsRow.y
                }
            }

            RowLayout {
                id: settingsRow

                ToolButton {
                    font.family: awesomesolid.name
                    text: fa.fa_magic
                    enabled: cam.connected

                    onClicked: cam.settingsAuto()
                }

                ToolButton {
                    font.family: awesomesolid.name
                    text: fa.fa_search
                    enabled: cam.connected

                    onClicked: cam.settingsMax()
                }
            }

        }
    }

    FileDialog {
        id: fileDialog
//        folder: StandardPaths.writableLocation(StandardPaths.HomeLocation)

        fileMode: FileDialog.SaveFile

        onFolderChanged: console.log(folder)

        onAccepted: {
            cam.saveRawFile(file)
        }

    }


    TimingOptions {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 10

        z: 5
    }



    Cppimg {
        id: image
        anchors.fill: parent
    }

    Camera {
        id: cam
        img: image
    }


}
