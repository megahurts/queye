#include "ueyecamera.h"

#include <QFile>

int UeyeCamera::openCamera()
{
    int r = 0;
    _cam = (HIDS)0;
    r = is_InitCamera(&_cam, 0);
    if (r != IS_SUCCESS) {
        if (r == IS_STARTER_FW_UPLOAD_NEEDED)
            qDebug() << "camera needs fw update";
        qDebug() << "unable to open camera (" << r << ")";

        _connected = false;
        return 1;
    }
    _connected = true;

    // set camera params
    r = is_SetDisplayMode(_cam, IS_SET_DM_DIB);
    if (r != IS_SUCCESS) {
        qDebug() << "unable to set display mode";
        closeCamera();
    }
    // file:///C:/Program%20Files/IDS/uEye/Help/uEye_Manual/index.html?sdk_allgemeines_farbformate.html
    r = is_SetColorMode(_cam, IS_CM_RGBA8_PACKED);
    if (r != IS_SUCCESS) {
        qDebug() << "unable to set color mode";
        closeCamera();
    }
    _bpp = 32;

    is_GetCameraInfo(_cam, &_ci);
    is_GetSensorInfo(_cam, &_si);

    _sizex = _si.nMaxWidth;
    _sizey = _si.nMaxHeight;
    _model = _si.strSensorName;

    allocateImages(16);

    is_SetGainBoost(_cam, IS_SET_GAINBOOST_ON);

    emit connectedChanged();
    emit sizeXChanged();
    emit sizeYChanged();
    emit modelChanged();
    emit timingChanged();
    emit gainChanged();
    return 0;
}

void UeyeCamera::allocateImages(int num)
{
    int r = 0;
    if (!_connected) {
        qDebug() << "cannot images before camera connected";
    }
    if (_nmems > 0)
        deallocateImages();

    _nmems = num;
    _ismems = new char*[_nmems];
    _ismemids = new int[_nmems];

    for (int i = 0; i < _nmems; i++) {
        r = is_AllocImageMem(_cam, _sizex, _sizey, _bpp, &_ismems[i], &_ismemids[i]);
        if (r != IS_SUCCESS) {
            qDebug() << "error alloc image " << i;
            deallocateImages();
        }

        r = is_AddToSequence(_cam, _ismems[i], _ismemids[i]);
        if (r != IS_SUCCESS) {
            qDebug() << "unable to add mem " << i << " to sequence";
        }
    }


    _dmem = new double[_sizex*_sizey];

    r = is_SetImageMem(_cam, _ismems[0], _ismemids[0]);
    if (r != IS_SUCCESS)
        qDebug() << "unable to set active image";

    resetAvgs();

}

void UeyeCamera::deallocateImages()
{
    is_ClearSequence(_cam);

    for (int i = 0; i < _nmems; i++) {
        is_FreeImageMem(_cam, (char*)_ismems[i], _ismemids[i]);
    }

    _nmems = 0;
    delete[] _ismems;
    delete[] _ismemids;
    delete[] _dmem;
}


void UeyeCamera::freezeImage()
{
    int r = 0;
    if (_capturing)
        endCapture();
    initEvent();

    // freeze and capture frame to memory
    r = is_SetExternalTrigger(_cam, IS_SET_TRIGGER_OFF);
    if (r != IS_SUCCESS) {
        qDebug() << "error setting trigger to off " << r;
        return;
    }

    r = is_FreezeVideo(_cam, IS_DONT_WAIT);
    if (r != IS_SUCCESS) {
        qDebug() << "error freezing image" << r;
        return;
    }

//    copyToImg(0);
}

void UeyeCamera::initEvent()
{
    if (_frameEvent != NULL)
        return;

    _frameEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (_frameEvent == NULL)
        qDebug() << "unable to create event";
    is_InitEvent(_cam, _frameEvent, IS_SET_EVENT_FRAME);
    is_EnableEvent(_cam, IS_SET_EVENT_FRAME);

    _frameNotifier.setHandle(_frameEvent);
    _frameNotifier.setEnabled(true);
}

void UeyeCamera::endEvent()
{
    is_DisableEvent(_cam, IS_SET_EVENT_FRAME);
    is_ExitEvent(_cam, IS_SET_EVENT_FRAME);
    CloseHandle(_frameEvent);
    _frameEvent = NULL;

    _frameNotifier.setEnabled(false);
}


void UeyeCamera::onFrameEvent()
{
    // new frame event!
    // find index of last frame read
    char *curmem, *lastmem;
    INT num;
    is_GetActSeqBuf (_cam, &num, &curmem, &lastmem);
    int i = 0;
    for (; i < _nmems && _ismems[i] != lastmem; i++) {}

    // lock frame and do something with it
    is_LockSeqBuf(_cam, i+1, _ismems[i]);

    // always update averages
    updateAvgs(i);

    // draw avg or live image depending on mode
    if (_viewAvgs)
        drawAvgs();
    else
        drawMem(i);

    is_UnlockSeqBuf(_cam, i+1, _ismems[i]);

    emit timingChanged();
    emit gainChanged();
}

void UeyeCamera::startCapture()
{
    int r = 0;
    initEvent();

    r = is_CaptureVideo(_cam, IS_DONT_WAIT);
    if (r != IS_SUCCESS) {
        qDebug() << "error starting capture" << r;
        return;
    }

    _capturing = true;
    capturingChanged();
//    is_FreezeVideo(hCam, IS_DONT_WAIT);

//    DWORD dwRet = WaitForSingleObject(_frameEvent, 1000);

//    if (dwRet == WAIT_TIMEOUT) {
//       qDebug() << "timeout";
//    }

//    else if (dwRet == WAIT_OBJECT_0) {
//      qDebug() << "wait ok";
//    }
}

void UeyeCamera::endCapture()
{
    if (!_connected)
        return;

    int r = 0;
    r = is_StopLiveVideo(_cam, IS_WAIT);
    if (r != IS_SUCCESS) {
        qDebug() << "error stopping video" << r;
    } else
        qDebug() << "video stopped";
    _capturing = false;
    capturingChanged();
}

void UeyeCamera::resetAvgs()
{
    if (!_connected)
        return;

    for (uint i = 0; i < _sizex*_sizey; i++)
        _dmem[i] = 0;
    _navgs = 0;

    navgsChanged();
}

//void UeyeCamera::drawMem(uint i)
//{
//    if (!_connected)
//        return;

//    if (!_img) {
//        qDebug() << "no img set";
//        return;
//    }
//    if (i >= (uint)_nmems) {
//        qDebug() << "invalid image index";
//        return;
//    }

//    for (uint j = 0; j < _sizex*_sizey; j++)
//        _ismems[i][3 + 4*j] = (char)255;

//    QImage im((uchar*)_ismems[i], _sizex, _sizey, 4*_sizex, QImage::Format_RGBA8888);

//    _img->setImage(im);
//}


void UeyeCamera::drawMem(uint i)
{
    if (!_connected)
        return;

    if (!_img) {
        qDebug() << "no img set";
        return;
    }
    if (i >= (uint)_nmems) {
        qDebug() << "invalid image index";
        return;
    }

    QImage im(_sizex, _sizey, QImage::Format_Grayscale8);
    for(uint j = 0; j < _sizey; j++){
        uchar *line = im.scanLine(j);
        for (uint k = 0; k < _sizex; k++) {
            line[k] = _ismems[i][(j*_sizex + k)*4];
        }
    }

    _img->setImage(im);
}


void UeyeCamera::updateAvgs(uint i)
{
    if (!_img) {
        qDebug() << "no img set";
        return;
    }
    if (i >= (uint)_nmems) {
        qDebug() << "invalid image index";
        return;
    }

    uint8_t *mem = (uint8_t*)_ismems[i];
    for (uint j = 0; j < _sizex*_sizey; j++)
        _dmem[j] += mem[0 + 4*j];
    _navgs += 1;

    navgsChanged();
}

void UeyeCamera::drawAvgs()
{
    QImage im(_sizex, _sizey, QImage::Format_Grayscale8);
    for(uint j = 0; j < _sizey; j++){
        uchar *line = im.scanLine(j);
        for (uint k = 0; k < _sizex; k++) {
            line[k] = (uchar)(_dmem[j*_sizex + k] / (double)_navgs);
        }
    }

    _img->setImage(im);
}

int UeyeCamera::closeCamera()
{
    if (_connected) {
        endEvent();
        deallocateImages();
        is_ExitCamera(_cam);
        _cam = (HIDS)0;
        _connected = false;
        emit connectedChanged();
    }

    _model = "No Camera";
    return 0;
}

void UeyeCamera::settingsAuto()
{
    if (!_connected)
        return;

    int r = 0;
    UEYE_AUTO_INFO autoInfo;
    r = is_GetAutoInfo(_cam, &autoInfo);
    if (r != IS_SUCCESS) {
        qDebug() << "failed to get auto info";
        return;
    }

    double val = 1;

    r = is_SetHardwareGain(_cam, 0, 0, 0, 0);
    if (r != IS_SUCCESS)
        qDebug() << "unable to set gain";

    if (autoInfo.AutoAbility & AC_GAIN) {
        // auto set master gain
        r = is_SetAutoParameter(_cam, IS_SET_ENABLE_AUTO_GAIN, &val, 0);
        if (r != IS_SUCCESS)
            qDebug() << "unable to set auto gain";
    }

    if (autoInfo.AutoAbility & AC_FRAMERATE) {
        // auto set master gain
        r = is_SetAutoParameter(_cam, IS_SET_ENABLE_AUTO_FRAMERATE, &val, 0);
        if (r != IS_SUCCESS)
            qDebug() << "unable to set auto framerate " << r;
    }

    emit timingChanged();
}

void UeyeCamera::settingsMax()
{
    if (!_connected)
        return;
    int r = 0;

    double val = 0;
    r = is_SetAutoParameter(_cam, IS_SET_ENABLE_AUTO_GAIN, &val, 0);
    if (r != IS_SUCCESS)
        qDebug() << "unable to disable auto gain";

    r = is_SetAutoParameter(_cam, IS_SET_ENABLE_AUTO_FRAMERATE, &val, 0);
    if (r != IS_SUCCESS)
        qDebug() << "unable to disable auto framerate";

    r = is_SetHardwareGain(_cam, 100, 100, 0, 0);
    if (r != IS_SUCCESS)
        qDebug() << "unable to set gain";


    double maxExp = 0;
    r = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE_RANGE_MAX, &maxExp, sizeof(double));
    if (r != IS_SUCCESS)
        qDebug() << "error getting max exposure";

    qDebug() << "max exposure " << maxExp;

    r = is_Exposure(_cam, IS_EXPOSURE_CMD_SET_EXPOSURE, &maxExp, sizeof(double));
    if (r != IS_SUCCESS)
        qDebug() << "error setting exposure";

    double exp = 0;
    r = is_Exposure(_cam, IS_EXPOSURE_CMD_GET_EXPOSURE, &exp, sizeof(double));
    if (r != IS_SUCCESS)
        qDebug() << "error getting exposure";
    qDebug() << "exposure set to " << exp << " ms";

    r = is_SetGainBoost(_cam, IS_SET_GAINBOOST_ON);
    if (r != IS_SUCCESS)
        qDebug() << "error setting gain boost";

    emit timingChanged();

}


void UeyeCamera::saveRawFile(const QUrl url)
{
    QFile f(url.toLocalFile());
    if (!f.open(QIODevice::WriteOnly)) {
        qDebug() << "error opening " << f.fileName();
        return;
    }

    double *normmem = new double[_sizex*_sizey];
    for (uint i = 0; i < _sizex*_sizey; i++)
        normmem[i] = _dmem[i] / (double)_navgs;

    int bytes = sizeof(double)*_sizex*_sizey;
    int bwritten = f.write((char*)normmem, bytes);
    if (bytes != bwritten)
        qDebug() << "only wrote " << bwritten << " of " << bytes;

    f.close();
    delete[] normmem;
}
