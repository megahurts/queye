#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

#include "cppimg.h"
#include "ueyecamera.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//    QQuickStyle::setStyle("Universal");

    qmlRegisterType<Cppimg>("Queye", 1, 0, "Cppimg");
    qmlRegisterType<UeyeCamera>("Queye", 1, 0, "Camera");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
