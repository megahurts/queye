#ifndef CPPIMG_H
#define CPPIMG_H

#include <QString>
#include <QQuickPaintedItem>
#include <QDebug>

#include <QBrush>
#include <QPainter>


static const char* colors[3] = {
    "#007430",
    "#303030",
    "#948326",
};
const int ncolors = 3;

class Cppimg : public QQuickPaintedItem
{
    Q_OBJECT
//    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)


public:
    explicit Cppimg(QObject *parent = nullptr)
      : _i(0), _pm(1,1)
    {
        (void)parent;
    }

    void paint(QPainter *painter) {
        painter->drawPixmap(0, 0, width(), height(), _pm);
//        painter->drawImage(0, 0, _im);
//        painter->drawImage(QRect(0, 0, width(), height()), _im);

//        QBrush brush(QColor("#007430"));

//        painter->setBrush(brush);
//        painter->setPen(Qt::NoPen);
//        painter->setRenderHint(QPainter::Antialiasing);

//        QSizeF itemSize = size();
//        painter->drawRoundedRect(0, 0, itemSize.width(), itemSize.height(), 10, 10);

    }

    Q_INVOKABLE void doSomething() {
        _i++;
        if (_i >= ncolors)
            _i = 0;

        update();
        qDebug() << _i;
    }

    void setImage(const QImage &i)
    {
        _pm = QPixmap::fromImage(i);
        //_im = i;
        update();
    }

signals:

private:
    int _i;
    QPixmap _pm;
    QImage _im;

};

#endif // CPPIMG_H
